import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DesignWithCssPracticeOneComponent} from "./design-with-css-practice-one/design-with-css-practice-one.component";

const routes: Routes = [
  {
    path: '', component: DesignWithCssPracticeOneComponent,
    children: [
      {path: 'one', component: DesignWithCssPracticeOneComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesignWithCssRoutingModule { }
