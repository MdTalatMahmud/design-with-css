import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DesignWithCssRoutingModule } from './design-with-css-routing.module';
import { DesignWithCssPracticeOneComponent } from './design-with-css-practice-one/design-with-css-practice-one.component';

@NgModule({
  declarations: [DesignWithCssPracticeOneComponent],
  imports: [
    CommonModule,
    DesignWithCssRoutingModule
  ]
})
export class DesignWithCssModule { }
