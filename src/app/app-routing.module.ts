import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/design-with-css'},
  {
    path: 'design-with-css',
    loadChildren: () => import('./design-with-css/design-with-css.module').then(m => m.DesignWithCssModule)
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
